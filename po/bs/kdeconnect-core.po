# Bosnian translation for bosnianuniversetranslation
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the bosnianuniversetranslation package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: bosnianuniversetranslation\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-11 00:47+0000\n"
"PO-Revision-Date: 2014-12-19 00:46+0000\n"
"Last-Translator: Samir Ribić <Unknown>\n"
"Language-Team: Bosnian <bs@li.org>\n"
"Language: bs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2014-12-19 06:45+0000\n"
"X-Generator: Launchpad (build 17286)\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#: backends/bluetooth/bluetoothpairinghandler.cpp:56
#: backends/lan/lanpairinghandler.cpp:52
#, kde-format
msgid "Canceled by other peer"
msgstr "Prekinuo drugi korisnik"

#: backends/bluetooth/bluetoothpairinghandler.cpp:65
#: backends/lan/lanpairinghandler.cpp:61
#, fuzzy, kde-format
#| msgid "Already paired"
msgid "%1: Already paired"
msgstr "Već uparen"

#: backends/bluetooth/bluetoothpairinghandler.cpp:68
#, fuzzy, kde-format
#| msgid "Pairing already requested for this device"
msgid "%1: Pairing already requested for this device"
msgstr "Uparivanje već zatraženo za ovaj uređaj"

#: backends/bluetooth/bluetoothpairinghandler.cpp:124
#: backends/lan/lanpairinghandler.cpp:106
#, kde-format
msgid "Timed out"
msgstr "Isteklo"

#: backends/lan/compositeuploadjob.cpp:82
#, kde-format
msgid "Couldn't find an available port"
msgstr ""

#: backends/lan/compositeuploadjob.cpp:121
#, kde-format
msgid "Failed to send packet to %1"
msgstr ""

#: backends/lan/compositeuploadjob.cpp:287
#, kde-format
msgid "Sending to %1"
msgstr ""

#: backends/lan/compositeuploadjob.cpp:287
#, kde-format
msgid "File"
msgstr ""

#: backends/lan/landevicelink.cpp:146 backends/lan/landevicelink.cpp:160
#, kde-format
msgid ""
"This device cannot be paired because it is running an old version of KDE "
"Connect."
msgstr ""

#: compositefiletransferjob.cpp:45
#, fuzzy, kde-format
#| msgid "Received incomplete file"
msgctxt "@title job"
msgid "Receiving file"
msgid_plural "Receiving files"
msgstr[0] "Primljena nepotpuna datoteka"
msgstr[1] "Primljena nepotpuna datoteka"
msgstr[2] "Primljena nepotpuna datoteka"

#: compositefiletransferjob.cpp:46
#, kde-format
msgctxt "The source of a file operation"
msgid "Source"
msgstr ""

#: compositefiletransferjob.cpp:47
#, kde-format
msgctxt "The destination of a file operation"
msgid "Destination"
msgstr ""

#: device.cpp:215
#, kde-format
msgid "Already paired"
msgstr "Već uparen"

#: device.cpp:220
#, kde-format
msgid "Device not reachable"
msgstr "Uređaj nije dostupan"

#: device.cpp:561
#, kde-format
msgid "SHA256 fingerprint of your device certificate is: %1\n"
msgstr ""

#: device.cpp:569
#, kde-format
msgid "SHA256 fingerprint of remote device certificate is: %1\n"
msgstr ""

#: filetransferjob.cpp:52
#, kde-format
msgid "Filename already present"
msgstr ""

#: filetransferjob.cpp:101
#, fuzzy, kde-format
#| msgid "Received incomplete file"
msgid "Received incomplete file: %1"
msgstr "Primljena nepotpuna datoteka"

#: filetransferjob.cpp:119
#, fuzzy, kde-format
#| msgid "Received incomplete file"
msgid "Received incomplete file from: %1"
msgstr "Primljena nepotpuna datoteka"

#: kdeconnectconfig.cpp:76
#, kde-format
msgid "KDE Connect failed to start"
msgstr ""

#: kdeconnectconfig.cpp:77
#, kde-format
msgid ""
"Could not find support for RSA in your QCA installation. If your "
"distribution provides separate packets for QCA-ossl and QCA-gnupg, make sure "
"you have them installed and try again."
msgstr ""

#: kdeconnectconfig.cpp:313
#, kde-format
msgid "Could not store private key file: %1"
msgstr ""

#: kdeconnectconfig.cpp:358
#, kde-format
msgid "Could not store certificate file: %1"
msgstr ""

#, fuzzy
#~| msgid "Receiving file over KDE-Connect"
#~ msgid "Receiving file %1 of %2"
#~ msgstr "Preuzimanje datoteke preko KDE-Veze"

#~ msgctxt "File transfer origin"
#~ msgid "From"
#~ msgstr "Od"

#~ msgctxt "File transfer destination"
#~ msgid "To"
#~ msgstr "Za"

#~ msgid "Error contacting device"
#~ msgstr "Greška pri povezivanju sa uređajem"

#~ msgid "Received incorrect key"
#~ msgstr "Primljen pogrešan ključ"

#~ msgid "Canceled by the user"
#~ msgstr "Poništeno od strane korisnika"

#~ msgid "Pairing request from %1"
#~ msgstr "Zahtjev za uparivanje od %1"

#~ msgid "Accept"
#~ msgstr "Prihvati"

#~ msgid "Reject"
#~ msgstr "Odbij"

#~ msgid "Incoming file exists"
#~ msgstr "Postoji dolazna datoteka"

#~ msgctxt "Device name that will appear on the jobs"
#~ msgid "KDE-Connect"
#~ msgstr "KDE-Connect"
